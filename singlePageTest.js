
const api = "https://uitest.free.beeceptor.com/usernames";
let list = [];

document.addEventListener("DOMContentLoaded", function(event) {
    if (!localStorage.getItem("itemsList")) {
        fetch(api)
        .then((rsp) => rsp.json())
        .then(function(data) {
            createListElements(data);
            localStorage.setItem("itemsList", JSON.stringify(data));
        });
    } else {
        list = JSON.parse(localStorage.getItem("itemsList"));
        createListElements(list);
    }
    isNotEmpty();
    document.getElementById("saveAllBtn").setAttribute("style", "display: none");

});

function createListElements(data) {
    let ul = document.querySelector("ul");
    list = data;

    for (i = 0; i <= list.length - 1; i++) {
        let li = document.createElement('li');   

        li.innerHTML = list[i].name + "<div><button type=\"button\" onclick=\"editItem()\">Edit</button> <button type=\"button\" onclick=\"deleteItem()\">Delete</button></div>";        
        li.setAttribute("value", i);
        ul.appendChild(li);   
    }
}

function editItem() {
    let currentItem = event.currentTarget.parentNode.parentElement;
    currentItem.innerHTML =" <input type=\"text\" value=\"" + list[currentItem.value].name + "\"> <div> <button type=\"button\" onclick=\"saveItem()\">Save</button></div>";  
}

function deleteItem() {
    let currentItem = event.currentTarget.parentNode.parentElement;
    list.splice(currentItem.value, 1); // remove element from list
    let newList = list;
    list = [];

    document.getElementById("list").innerHTML = ""; // remove all li from DOM
    createListElements(newList);
}

function saveItem() {
    let currentItem = event.currentTarget.parentNode.parentElement;
    let currentInputValue = currentItem.getElementsByTagName("input")[0].value;
    list[currentItem.value].name = currentInputValue;
    currentItem.innerHTML = list[currentItem.value].name + "<div> <button type=\"button\" onclick=\"editItem()\">Edit</button> <button type=\"button\" onclick=\"deleteItem()\">Delete</button></div>";     
}

function isNotEmpty () {
    let addUserInput = document.getElementById("addUser");
    let addUserBtn = document.getElementById("addUserBtn");

    if (!addUserInput.value) {
        addUserBtn.setAttribute('disabled', true);
    } else {
        addUserBtn.removeAttribute('disabled');
    }
}

function addNewUser() {
    let addUserInput = document.getElementById("addUser");
    let ul = document.querySelector("ul");
    let li = document.createElement('li');   

    list.push({name: addUserInput.value});

    li.innerHTML = list[list.length - 1].name + "<div> <button type=\"button\" onclick=\"editItem()\">Edit</button> <button type=\"button\" onclick=\"deleteItem()\">Delete</button></div>";        
    li.setAttribute("value", list.length - 1);
    ul.appendChild(li); 

    // only show edit all and delete all buttons if list is not empty
    if (list.length > 0) {
        document.getElementById("editDeleteBtns").setAttribute("style", "display: block");
    }

    removeInputValue();
}

function removeInputValue() {
    let addUserInput = document.getElementById("addUser");
    addUserInput.value = "";
    isNotEmpty();
}

function editAll() {
    document.getElementById("editDeleteBtns").setAttribute("style", "display: none");
    document.getElementById("saveAllBtn").setAttribute("style", "display: block");

    let li = document.querySelector("ul").children;
    for(i = 0; i <= li.length - 1; i++) {
        li[i].innerHTML =" <input type=\"text\" value=\"" + list[li[i].value].name + "\">";  
    }
}

function saveAll() {
    let liList = document.getElementById("list").children;
    let newList = [];

    for(i = 0; i <= liList.length - 1; i++) {
        newList.push({name: liList[i].querySelector("input").value});
    }
    document.getElementById("list").innerHTML = ""; // remove all li from DOM
    list = newList;
    createListElements(list);

    document.getElementById("editDeleteBtns").setAttribute("style", "display: block");
    document.getElementById("saveAllBtn").setAttribute("style", "display: none");
}

function deleteAll() {
    list = [];
    document.getElementById("list").innerHTML = ""; // remove all li from DOM
    document.getElementById("editDeleteBtns").setAttribute("style", "display: none");
}